﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GamePlay : MonoBehaviour
{
    public static int money;
    public Slider hpbar;
    public Slider happybar;
    public static float currentHP, maxHP, currentHappy, maxHappy;
    public Text daytext;
    public Text moneytext;
    public Text weather;
    public Text flavor;
    int weathercount;
    int weatherrandom;
    int panelcount = 1;
    int daycount = 1;
    int flavorcount;
    int flavorrandom;
    public GameObject panel1;
    public GameObject panel2;
    public GameObject panel3;
    public GameObject panel4;
    public GameObject panel5;
    public GameObject panel6;
    public GameObject day6panel1;
    public GameObject day6panel2;
    public GameObject day6panel3;
    public GameObject buttonPlay;
    public GameObject day7_eventTime;
    public GameObject down_Icon;
    public GameObject sun;
    public GameObject rain;
    public GameObject flavorpanel;
    public GameObject panel6_good;
    public GameObject panel6_bad;

    // Start is called before the first frame update
    void Start()
    {
        money = 100;
        maxHappy = 100f;
        maxHP = 100f;
        currentHP = 50f;
        currentHappy = 30f;
        happybar.value = CalculateHappy();
        hpbar.value = CalculateHP();
        

    }
    //ฟังก์ชันสำหรับให้ปุ่มทำการเล่นeventที่เกิดขึ้นโดยสามารถเกดล่นได้หลายeventในปุ่มเดียว
    public void PlayEvent()
    {
        if (panelcount == 1)
        {
            buttonPlay.SetActive(false);
            panel6.SetActive(false);
            if (daycount >= 2)
            {
                money += Random.Range(30, 50);
            }
            flavorcount = 1;
            if (flavorcount==1)
            {
                flavorrandom = Random.Range(1,3);
                if (flavorrandom==2)
                {
                    flavor.text = "อยากกินของหวาน";
                }
                else
                {
                    flavor.text = "อยากกินอาหารเพื่อสุขภาพ";
                }
            }
            weathercount = 1;
            if (weathercount==1)
            {
                weatherrandom = Random.Range(1,3);
                if (weatherrandom==2)
                {
                    weather.text = "Rain";
                    rain.SetActive(true);
                    sun.SetActive(false);
                }
                else
                {
                    weather.text = "Sunny";
                    sun.SetActive(true);
                    rain.SetActive(false);
                }
            }
            flavorpanel.SetActive(true);
            panel1.SetActive(true);
            panelcount += 1;
        }
        else if (panelcount == 2)
        {
            panel2.SetActive(true);
            panelcount += 1;
        }
        else if (panelcount == 3)
        {
            panel3.SetActive(true);
            int randomcount = Random.Range(1, 3);
            if (randomcount == 2)
            {
                panelcount += +1;
            } else
            {
                panelcount += +2;
            }

        } else if (panelcount == 4)
        {
            panel4.SetActive(true);
            panelcount += 1;
        }
        else if (panelcount == 5)
        {
            panel5.SetActive(true);
            panelcount += 1;
            flavorcount = 0;
            flavorpanel.SetActive(false);
        }
        else if (panelcount == 6)
        {
            panel6.SetActive(true);
            if(currentHappy <=50 || currentHP <=50)
            {
                panel6_bad.SetActive(true);
            }
            else
            {
                panel6_good.SetActive(true);
            }
            panelcount -= 5;
            buttonPlay.SetActive(true);
        }
        if (daycount == 6)
        {
            buttonPlay.SetActive(false);
            PlayDay6Event();
        }

    }
    
    //ฟังก์ชันสำหรับการขึ้นวันใหม่
    public void StartNewDay()
    {
        daycount += 1;
        if (currentHappy <= 0)
        {
            currentHP -= 5;
        }
    }
    //ฟังก์ชันสำหรับตัวเลือกต่างๆ ที่เกิดขึ้น

    //ตัวเลือกไป รร.
    public void Choice1panel1()
    {
        money -= 10;
        if (weatherrandom==2)
        {
            currentHappy -=5;
            
        }
        else
        {
            currentHappy += Random.Range(1, 5);
        }
        panel1.SetActive(false);
        PlayEvent();
    }
    public void Choice2panel1()
    {
        money -= 20;
        if (weatherrandom == 2)
        {
            currentHappy += 10;
        }
        else
        {
            currentHappy += Random.Range(5, 20);
        }
        panel1.SetActive(false);
        PlayEvent();
    }
    public void Choice3panel1()
    {
        if (weatherrandom == 2)
        {
            currentHP -=30;
            currentHappy -=45;
        }
        else 
        {
            currentHP += Random.Range(20,40);
            currentHappy -= Random.Range(10,20);
        }
        panel1.SetActive(false);
        PlayEvent();
    }
    //ตัวเลือกอาหารเช้า
    public void Choice1panel2() // แฮมไข่
    {
        money -= 15;
        currentHappy += Random.Range(5, 15);
        currentHP += Random.Range(5, 15);
        panel2.SetActive(false); 
        PlayEvent();
    }
    public void Choice2panel2() // หมู กะ โจ๊ก
    {
        money -= 10;
        currentHappy -=5;
        currentHP += 5;
        panel2.SetActive(false);
        PlayEvent();
    }
    public void Choice3panel2() // แพนเค้ก
    {
        money -= 20;
        if (flavorrandom==2)
        {
            currentHappy += 20;
            currentHP -= 10;
            flavorpanel.SetActive(false);
        }
        else
        {
            currentHappy += Random.Range(10, 25);
            currentHP -= 10;
        }
        panel2.SetActive(false);
        PlayEvent();
    }
    public void Choice4panel2() // ไม่กินอะไรเลย
    {       
        currentHappy -= Random.Range(20,50);
        currentHP -= Random.Range(10,30);
        panel2.SetActive(false);
        PlayEvent();
    }
    //ตัวเลือกอาหารกลางวัน
    public void Choice1panel3()
    {
        money -= 20;
        currentHappy += Random.Range(5, 15);
        currentHP += Random.Range(5, 15);
        panel3.SetActive(false);
        PlayEvent();
    }
    public void Choice2panel3()
    {
        money -= 5;
        currentHappy -= 5;
        currentHP += 5;
        panel3.SetActive(false);
        PlayEvent();
    }
    public void Choice3panel3()
    {
        money -= 25;
        if (flavorrandom!=2)
        {
            currentHappy += 15;
            currentHP += Random.Range(10, 25);
            flavorpanel.SetActive(false);
        }
        else
        {
            currentHappy -= 10;
            currentHP += Random.Range(10, 25);
        }
        panel3.SetActive(false);
        PlayEvent();
    }
    public void Choice4panel3()
    {
        money -= 25;
        currentHappy += Random.Range(10, 25);
        currentHP -= 10;
        panel3.SetActive(false);
        PlayEvent();
    }
    //ตัวเลือกไอเทมพิเศษ
    public void Choice1panel4()
    {
        money -= 30;
        currentHP += Random.Range(15, 25);
        panel4.SetActive(false);
        PlayEvent();
    }
    public void Choice2panel4()
    {
        money -= 30;
        currentHappy += Random.Range(15, 25);
        panel4.SetActive(false);
        PlayEvent();
    }
    public void Choice3panel4()
    {
        money -= 20;
        currentHP += Random.Range(10,20);
        currentHappy -= Random.Range(10,20);
        panel4.SetActive(false);
        PlayEvent();
    }
    public void Choice4panel4()
    {
        money -= 20;
        currentHP -= Random.Range(10, 20);
        currentHappy += Random.Range(10, 20);
        panel4.SetActive(false);
        PlayEvent();
    }
    public void Choice5panel4()
    {
        currentHP -= Random.Range(10, 20);
        currentHappy -= Random.Range(10, 20);
        panel4.SetActive(false);
        PlayEvent();
    }
    //ตัวเลือกกลับบ้าน
    public void Choice1panel5()
    {
        money -= 10;
        if (weatherrandom == 2)
        {
            currentHappy -= 5;
        }
        else
        {
            currentHappy += Random.Range(1, 5);
        }
        panel5.SetActive(false);
        weathercount = 0;
        StartNewDay();
        PlayEvent();
    }
    public void Choice2panel5()
    {
        money -= 20;
        if (weatherrandom == 2)
        {
            currentHappy += 10;
        }
        else
        {
            currentHappy += Random.Range(5, 20);
        }
        panel5.SetActive(false);
        weathercount = 0;
        StartNewDay();
        PlayEvent();
    }
    public void Choice3panel5()
    {
        if (weatherrandom == 2)
        {
            currentHP -= 30;
            currentHappy -= 45;
        }
        else
        {
            currentHP += Random.Range(20, 40);
            currentHappy -= Random.Range(10, 20);
        }
        panel5.SetActive(false);
        weathercount = 0;
        StartNewDay();
        PlayEvent();
    }
    //ฟังก์ชันสำหรับตัวเลือกต่างๆ ที่เกิดขึ้น

    //อีเว้น พิเศษ
    public void PlayDay6Event()
    {
        money += 50;
        if (panelcount == 1)
        {
            panel6.SetActive(false);
            day6panel1.SetActive(true);
            panelcount += 1;
        }
        else if (panelcount == 2)
        {
            day6panel2.SetActive(true);
            panelcount += 1;
        }
        else if (panelcount == 3)
        {
            day6panel3.SetActive(true);
            panelcount += 1;
        }
        else if (panelcount == 4)
        {
            panel6.SetActive(true);
            panelcount -= 3;
        }

    }
    public void Choice1SPDay6Panel1()
    {
        money -= 10;
        currentHappy += 5;
        day6panel1.SetActive(false);
        PlayDay6Event();
    }
    public void Choice2SPDay6Panel1()
    {
        money -= 15;
        currentHappy += 20;
        currentHP += 10;
        day6panel1.SetActive(false);
        PlayDay6Event();
    }
    public void Choice3SPDay6Panel1()
    {
        currentHP += 10;
        currentHappy -= 5;
        day6panel1.SetActive(false);
        PlayDay6Event();
    }
    //ไอเทม event
    public void Choice1SPDay6Panel2()
    {
        money -= Random.Range(29, 61);
        currentHappy += 25;
        day6panel2.SetActive(false);
        PlayDay6Event();
    }
    public void Choice2SPDay6Panel2()
    {
        money -= 10;
        currentHappy -= 30;
        day6panel2.SetActive(false);
        PlayDay6Event();
    }
    //กลับบ้าน อีเว้น
    public void Choice1SPDay6Panel3()
    {
        money -= 10;
        currentHappy += 5;
        day6panel3.SetActive(false);
        //day7_eventTime.SetActive(true);
        StartNewDay();

    }
    public void Choice2SPDay6Panel3()
    {
        money -= 40;
        currentHappy += 15;
        day6panel3.SetActive(false);
        //day7_eventTime.SetActive(true);
        StartNewDay();

    }
    public void Choice3SPDay6Panel3()
    {
        currentHP += 10;
        currentHappy -= 5;
        day6panel3.SetActive(false);
        //day7_eventTime.SetActive(true);
        StartNewDay();

    }


    //ฟังก์ชันสำหรับการคำนวณค่า health
    float CalculateHP()
    {
        return currentHP / maxHP;
    }
    //ฟังก์ชันสำหรับการคำนวณค่า happy
    float CalculateHappy()
    {
        return currentHappy/maxHappy;
    }
    //ฟังก์ชันสำหรับการเก็บข้อมูลเพิ่อนำไปใช้ในซีนอื่นๆ
    public void SaveScore()
    {
        PlayerPrefs.SetInt("Currentmoney", money);
        PlayerPrefs.SetFloat("Health", currentHP);
        PlayerPrefs.SetFloat("Happy", currentHappy);
    }
    // Update is called once per frame
    void Update()
    {
        happybar.value = CalculateHappy(); //ใช้สำหรับค่า happy แสดงใน ui slider
        hpbar.value = CalculateHP(); //ใช้สำหรับค่า health แสดงใน ui slider
        moneytext.text = "" + money.ToString(); //ใช้สำหรับการให้ค่า money แสดงใน ui text
        daytext.text= "Day: " + daycount.ToString(); //ใช้สำหรับการให้ค่า daycount แสดงใน ui text
        //ฟังก์ชันสำหรับการข้ามซีนเมื่อผู้เล่นเล่นครบ 7 วันแล้ว
       
        if (daycount == 7)
        {
            SaveScore();
            SceneManager.LoadScene("sumary");
        }
        if (money<=0)
        {
            SaveScore();
            SceneManager.LoadScene("sumary");
        }
        if (currentHappy<=0)
        {
            SaveScore();
            SceneManager.LoadScene("sumary");
        }
        if (currentHP<=0)
        {
            SaveScore();
            SceneManager.LoadScene("sumary");
        }
        

    }
}
