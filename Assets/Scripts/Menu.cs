﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
    //ฟังก์ชันสำหรับการข้ามซีน
   public void GoGameplay()
    {
        SceneManager.LoadScene("gameplay");
    }
    public void GoTutorial()
    {
        SceneManager.LoadScene("tutorial");
    }
    public void GoMenu()
    {
        SceneManager.LoadScene("Menu");
    }
}
