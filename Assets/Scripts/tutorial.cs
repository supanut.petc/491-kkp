﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class tutorial : MonoBehaviour
{
    public GameObject text1,text2,text3,text4,text5,text6,text7,text8,text9,text89,text10;
    public GameObject PlayButton;
    public GameObject t8Button1,t8Button2;

    // Start is called before the first frame update
    void Start()
    {
        
    }
    public void gotoText2()
    {
        text1.SetActive(false);
        text2.SetActive(true);
        PlayButton.SetActive(true);
    }
    public void gotoText4()
    {
        text3.SetActive(false);
        text4.SetActive(true);
    }
    public void closeText5()
    {
        text5.SetActive(false);
    }
    public void closeText6()
    {
        text6.SetActive(false);
    }
    public void closeText7()
    {
        text7.SetActive(false);
    }
    public void goToText9()
    {
        text8.SetActive(false);
        text9.SetActive(true);
        t8Button1.SetActive(false);
        t8Button2.SetActive(true);
    }
    public void closeText9()
    {
        text89.SetActive(false);
    }
    public void goToGameplayScene()
    {
        SceneManager.LoadScene("gameplay");
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
