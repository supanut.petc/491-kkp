﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaySounEffect : MonoBehaviour
{
    public AudioSource myFx;
    public AudioClip clickFx;
    public AudioClip moneyFX;
    public AudioClip moneyheistFX;
    public AudioClip lowmoneyFX;

    //ฟังก์ชันสำหรับการเล่นเสียง
    public void ClickSound()
    {
        myFx.PlayOneShot(clickFx);
    }
    //ฟังก์ชันสำหรับการเล่นเสียง
    public void MoneySound()
    {
        myFx.PlayOneShot(moneyFX);
    }

    public void MoneyheistSound()
    {
        myFx.PlayOneShot(moneyheistFX);
    }

    public void LowMoneySound()
    {
        myFx.PlayOneShot(lowmoneyFX);
    }
}
