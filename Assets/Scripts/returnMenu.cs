﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class returnMenu : MonoBehaviour
{
    public GameObject returnPanal;

    public void ClicktoReturnpanal()
    {
        returnPanal.SetActive(true);
    }

    public void ClicktoClosepanal()
    {
        returnPanal.SetActive(false);
    }

    public void ClicktoMenupanal()
    {
        SceneManager.LoadScene("Menu");
    }
}
