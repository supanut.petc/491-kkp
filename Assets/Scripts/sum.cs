﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class sum : MonoBehaviour
{
    public Text MoneyText;
    public Text HealthText;
    public Text HappyText;
    public Text GradeText;
    // Start is called before the first frame update
    void Start()
    {
        calculategrade();
        MoneyText.text = "" + PlayerPrefs.GetInt("Currentmoney");
        HealthText.text = "" + PlayerPrefs.GetFloat("Health");
        HappyText.text = "" + PlayerPrefs.GetFloat("Happy");
    }
    public void calculategrade()
    {
        if (PlayerPrefs.GetInt("Currentmoney") >= 70 && PlayerPrefs.GetFloat("Health") >= 70 && PlayerPrefs.GetFloat("Happy") >= 70)
        {
            GradeText.text = "A";
        }
        else if (PlayerPrefs.GetInt("Currentmoney") >= 70 && PlayerPrefs.GetFloat("Health") >=70 && PlayerPrefs.GetFloat("Happy") < 70 && PlayerPrefs.GetFloat("Happy") > 0 || PlayerPrefs.GetInt("Currentmoney") >= 70 && PlayerPrefs.GetFloat("Health") < 70 && PlayerPrefs.GetFloat("Health") > 0 && PlayerPrefs.GetFloat("Happy") >= 70)
        {
            GradeText.text = "B";
        }
        else if (PlayerPrefs.GetInt("Currentmoney") < 70 && PlayerPrefs.GetInt("Currentmoney") > 0 && PlayerPrefs.GetFloat("Health") >= 70 && PlayerPrefs.GetFloat("Happy") >= 70 || PlayerPrefs.GetInt("Currentmoney") >= 70 && PlayerPrefs.GetFloat("Health") < 70 && PlayerPrefs.GetFloat("Health")>0 && PlayerPrefs.GetFloat("Happy") < 70 && PlayerPrefs.GetFloat("Happy")>0)
        {
            GradeText.text = "B";
        }
        else if (PlayerPrefs.GetInt("Currentmoney") >= 70 && PlayerPrefs.GetFloat("Health") < 70 && PlayerPrefs.GetFloat("Health")>0 && PlayerPrefs.GetFloat("Happy") >= 70 || PlayerPrefs.GetInt("Currentmoney") < 70 && PlayerPrefs.GetInt("Currentmoney")>0 && PlayerPrefs.GetFloat("Health") >= 70 && PlayerPrefs.GetFloat("Happy") >= 70)
        {
            GradeText.text = "B";
        }
        else if (PlayerPrefs.GetInt("Currentmoney") >= 70 && PlayerPrefs.GetFloat("Health") < 70 && PlayerPrefs.GetFloat("Health") > 0 && PlayerPrefs.GetFloat("Happy") < 70 && PlayerPrefs.GetFloat("Happy") > 0 || PlayerPrefs.GetInt("Currentmoney") < 70 && PlayerPrefs.GetInt("Currentmoney") > 0 && PlayerPrefs.GetFloat("Health") >= 70 && PlayerPrefs.GetFloat("Happy") < 70 && PlayerPrefs.GetFloat("Happy") > 0 || PlayerPrefs.GetInt("Currentmoney") < 70 && PlayerPrefs.GetFloat("Currentmoney") > 0 && PlayerPrefs.GetFloat("Health") < 70 && PlayerPrefs.GetFloat("Health") > 0 && PlayerPrefs.GetFloat("Happy") >= 70)
        {
            GradeText.text = "C";
        }
        else if (PlayerPrefs.GetInt("Currentmoney") < 70 && PlayerPrefs.GetInt("Currentmoney") > 0 && PlayerPrefs.GetFloat("Health") < 50 && PlayerPrefs.GetFloat("Health") > 0 && PlayerPrefs.GetFloat("Happy") < 50 && PlayerPrefs.GetFloat("Happy") > 0)
        {
            GradeText.text = "D";
        }
        else if (PlayerPrefs.GetInt("Currentmoney") <= 0|| PlayerPrefs.GetFloat("Health") <= 0|| PlayerPrefs.GetFloat("Happy") <= 0)
        {
            GradeText.text = "F";
        }
       

    }
}